# Rustik Framework

[![version](https://img.shields.io/badge/repo-master-yellow.svg)](https://bitbucket.org/opirrello/rustik-framework) [![download](https://img.shields.io/badge/packagist-last-green.svg)](https://packagist.org/packages/rustik/rustik-framework) ![download](https://img.shields.io/packagist/dm/rustik/rustik-framework?color=lightgrey)

A rustic, simple and robust **PHP Framwork**. Easy to use and to improve.
Is a simple project to develop simple web/apps.

### Installing Rustik
to install it, you can download from here or you can use **[composer](https://getcomposer.org)**.
If you don't have composer installed in your desktop, you must download it from **https://getcomposer.org/download/**

##### Installing with composer **(recommended)**
After downloading **[composer](https://getcomposer.org)** you should position yourself in your rustik installation folder.
Once you are there, you must run this:

```
composer create-project rustik/framework --prefer-dist <my-proyect>
```
In this command you are telling to **composer** to download the package related to **rustik**, and with the __--prefer-dist__ you are telling to composer that you want the instalation is made under the **<my-proyect>** name.

Run the command and **composer** will make the magic for you.


