<?php
function msleep($time) {
    usleep($time * 1000000);
}

function file_copy($src_file, $des_file, $prefix='') {
   echo "\033[39m$prefix copy of \e[32m".basename($src_file)."\e[39m... ";
   msleep(.2);
   $status = @copy($src_file, $des_file);

   if ($status) 
        echo "\033[32mDONE\e[39m\r\n";
   else echo "\033[31mFAILED (check permission in this file)\e[39m\r\n";

   return $status;
}

function dir_copy($src_dir, $des_dir, $prefix='') {
   $status = true;
   $error  = false;

   if (is_dir($src_dir)) {
      $dir=opendir($src_dir);

       $cantFilesOk  = 0;
       $cantFilesBad = 0;
       $cantErrors   = 0;
       $subdirError = false;

       while (($file = readdir($dir)) !== false) {
           if ($file === '.' || $file === '..') {
               continue;
           }

           if (is_dir("$src_dir".DIRECTORY_SEPARATOR."$file") === true) {
               if (is_dir("$des_dir".DIRECTORY_SEPARATOR."$file") === false) {
                   echo "\033[39m$prefix Creating directory $des_dir".DIRECTORY_SEPARATOR."$file...";
                   $status = @mkdir("$des_dir/$file");

                   if ($status)
                        echo "\033[32m DONE\e[39m\r\n";
                   else echo "\033[31m FAILED\e[39m\r\n";
               }
               else $status = true;

               if ($status) {
                   $base_dir = str_replace(COREDIR,'', $des_dir);

                   echo "\033[32m$prefix Updating files into $des_dir".DIRECTORY_SEPARATOR."$file... \e[39m\r\n";
                   $status = dir_copy($src_dir.DIRECTORY_SEPARATOR.$file, $des_dir.DIRECTORY_SEPARATOR.$file, $prefix.'---');
               }
           }
           else {
              msleep(.2);
              $status = file_copy("$src_dir".DIRECTORY_SEPARATOR."$file", "$des_dir".DIRECTORY_SEPARATOR."$file", $prefix);

              if ($status) 
                   $cantFilesOk++;
              else $cantFilesBad++;
           }

           if (!$status) {
              if (!$subdirError) {
                 $cantErrors++;
              }

              $error = true;
           }

           $subdirError = false;
       }
   
       if ($error && ($cantErrors || $cantFilesBad))
                       echo "\033[33m$prefix $cantFilesOk file/s copied in ".DIRECTORY_SEPARATOR.basename($des_dir)." \e[31m($cantErrors errors occured / $cantFilesBad files cannot be copied)\e[39m\r\n";
       elseif($error)  echo "\033[33m$prefix $cantFilesOk file/s copied in ".DIRECTORY_SEPARATOR.basename($des_dir)." \e[31m(some errors occured in subdirectories)\e[39m\r\n";
       else            echo "\033[33m$prefix $cantFilesOk file/s copied in ".DIRECTORY_SEPARATOR.basename($des_dir)." \e[39m\r\n";
       
       closedir($dir);
   }
   else {
      echo "\e[31m$prefix $src_dir doesn't exist or there are some access restriction\e[39m\r\n";
      $status = false;
   }

   return !$error;
}

/***** updating rustik core *****/
$baseDir = getcwd();
$baseData= ['index.php','gofer'];

$coreDir = "$baseDir".
           DIRECTORY_SEPARATOR."core";
$vendDirS= "$baseDir".
           DIRECTORY_SEPARATOR."vendor".
           DIRECTORY_SEPARATOR."rustik".
           DIRECTORY_SEPARATOR."core";
$vendDir = "$baseDir".
           DIRECTORY_SEPARATOR."vendor".
           DIRECTORY_SEPARATOR."rustik".
           DIRECTORY_SEPARATOR."core".
           DIRECTORY_SEPARATOR."core";
$fnSrcDir= "$baseDir".
           DIRECTORY_SEPARATOR."core".
           DIRECTORY_SEPARATOR."application".
           DIRECTORY_SEPARATOR."layouts".
           DIRECTORY_SEPARATOR."fonts".
           DIRECTORY_SEPARATOR."html2pdf";
$fnVenDir= "$baseDir".
           DIRECTORY_SEPARATOR."vendor".
           DIRECTORY_SEPARATOR."tecnickcom".
           DIRECTORY_SEPARATOR."tcpdf".
           DIRECTORY_SEPARATOR."fonts";

define('COREDIR', $coreDir);

echo "\033[33mUpdating Rustik core...\e[39m\r\n";
sleep(2);

if (!file_exists($vendDirS.DIRECTORY_SEPARATOR.'.ver')) {
   echo "\033[32mRustik Core is up to date. Any file updated.\e[0m\r\n";
   unlink($vendDirS.DIRECTORY_SEPARATOR.'.ver');
   exit;
}

foreach($baseData as $baseFile)
file_copy($vendDirS.DIRECTORY_SEPARATOR.$baseFile, $baseDir.DIRECTORY_SEPARATOR.$baseFile);

if (dir_copy($vendDir, $coreDir))
     echo "\033[32mRustik Core successfully installed. All core files updated.\e[0m\r\n";
else echo "\033[41m\e[39m Rustik Core instalation failed. Some files or directories could not be updated. \e[0m\r\n";

echo "\r\n";

echo "\033[33mUpdating Rustik fonts for PDF library...\e[39m\r\n";
msleep(1);
if (!is_dir($fnSrcDir)) {
   echo "\033[32mNo fonts to be updated.\e[0m\r\n";
}
else {
if (dir_copy($fnSrcDir, $fnVenDir))
     echo "\033[32mAll font files where successfully updated.\e[0m\r\n";
else echo "\033[41m\e[39m Some font files could not be updated. \e[0m\r\n";
}
//cp -v ./core/application/layouts/fonts/html2pdf/* ./vendor/tecnickcom/tcpdf/fonts

