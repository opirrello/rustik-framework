<?php
class UserModel extends Model {
   protected $_primary_key = array('id');

   public function aliasExists($alias) {
      $qry = "SELECT COUNT(*) AS hay FROM user WHERE alias = '$alias'";
      $result = $this->execute($qry);

      return $result[0]['exists'] != 0;
   }

   public function activateUser($idUsuario) {
      $qry = "UPDATE user SET status = 'A' WHERE id = ?";

      $this->execute("BEGIN");
      $updated = $this->execute($qry, array($idUsuario));

      if ($updated == -1) {
         $this->execute("ROLLBACK");
         return false;
      }
      else {
         $this->execute("COMMIT");
         return true;
      }
   }

   public function isActiveUser($alias, $pass) {
      $method = (defined('RF_SCOD_ENC_METHOD'))?RF_SCOD_ENC_METHOD:'sha1';
      $salt   = (defined('RF_SCOD_ENC_SALT'))?RF_SCOD_ENC_SALT:'';
      $sltPass= $salt . $pass;
      $encPass= hash($method, $sltPass);
      $qry    = "SELECT COUNT(*) AS hay FROM user WHERE alias = '$alias' AND password = '$encPass' AND status = 'A'";
      $result = $this->execute($qry);

      return $result[0]['exists'] != 0;
   }

   public function getBasicData($user_alias) {
      if (is_numeric($user_alias))
           $field = 'user.id';
      else $field = 'alias';

      $profId = TM_USRPROFAFF;

      $qry = "SELECT user.id, alias, first_name, last_name, profile_id, description, shortname, labelcolor, context, status, email
                FROM user INNER JOIN profile WHERE $field = '$user_alias' AND profile_id = profile.id";
      $result = $this->execute($qry);

      return $result;
   }

   public function hasPermissionFor($procCode) {
      if (isset($_SESSION['authInfo'])) {
         $userInfo = unserialize($_SESSION['authInfo']);

         if (is_array($procCode)) { 
            $procCond = "IN ('". implode("','", $procCode). "')";
            $procSelt = "shortname";
         }
         else {
            $procCond = "= '$procCode'";
            $procSelt = "COUNT(*) AS able";
         }

         $qry = "SELECT COUNT(*) AS able
                   FROM permission PER
                   JOIN process PRO 
                     ON PER.process_id = PRO.id
                    AND profile_id = ".$userInfo['profile_id']."
                    AND shortname = '$procCode'";
         $result = $this->execute($qry);

         if (is_array($procCode)) { 
            foreach($result as $fieldData) {
               $list[] = $fieldData[$procSelt];
            }

            return $list;
         }
         else {
            $result = $result[0];
            return $result['able'] != 0;
         }
      }
      else{
         return false;
      }
   }

   public function editUserPassByPost($userInfo, $updaterAlias) {
      $rollback = false;

      $this->execute("BEGIN");

      $method = (defined('RM_SCOD_ENC_METHOD'))?RM_SCOD_ENC_METHOD:'sha1';
      $salt   = (defined('RM_SCOD_ENC_SALT'))?RM_SCOD_ENC_SALT:'';
      $sltPass= $salt . $_POST['password'];
      $hshPass= hash($method, $sltPass);

      $updData = array(
         'password'=>$hshPass,
         'usu_modi'=>$updaterAlias,
         'fec_modi'=>'NOW()'
      );

      $edited = $this->updateBy($updData, array('id'=>$userInfo['id']));

      if ($edited) {
         $this->execute("COMMIT");
         return true;
      }
      else {
         $this->execute("ROLLBACK");
         return false;
      }
   }

   public function updateUserByPost() {
      if ($_POST['password'] != USER_EDIT_FAKEPASS) {
         $method  = (defined('RM_SCOD_ENC_METHOD'))?RM_SCOD_ENC_METHOD:'sha1';
         $salt    = (defined('RM_SCOD_ENC_SALT'))?RM_SCOD_ENC_SALT:'';
         $sltPass = $salt . $_POST['password'];
         $encPass = hash($method, $sltPass);
      }

      foreach($_POST as $fieldname => $fieldvalue) {
         switch($fieldname) {
         case 'firstname': $updSet['first_name'] = $fieldvalue; break;
         case 'lastname' : $updSet['last_name']  = $fieldvalue; break;
         case 'sexo'     : $updSet['sex']        = $fieldvalue; break;
         case 'phone'    : $updSet['phone']      = $fieldvalue; break;
         case 'cellphone': $updSet['cellphone']  = $fieldvalue; break;
         case 'email'    : $updSet['email']      = $fieldvalue; break;
         case 'alias'    : $updSet['alias']      = $fieldvalue; break;
         case 'password' : if ($_POST['password'] != USER_EDIT_FAKEPASS) $updSet['password'] = $encPass; break;
         case 'profile'  : $updSet['profile_id'] = $fieldvalue; break;
         case 'status'   : $updSet['status']     = $fieldvalue; break;
         case 'context'  : $updSet['context']    = $fieldvalue; break;
         case 'acronym'  : $updSet['acronym']    = $fieldvalue; break;
         }
      }

      $updSet['update_date'] = 'NOW()';
      $updSet['update_user'] = $_POST['update_user'];

      $this->execute("BEGIN");
     
      $updated = $this->updateBy($updSet, array($_POST['pk']));
      //echo $this->_dbhandler->getLastQry();
      
      if ($this->_dbhandler->getLastError()) {
         $response['error']['code']    = $this->_dbhandler->getLastError();
         $response['error']['message'] = $this->_dbhandler->getLastErrorMessage();

         if (strpos($this->_dbhandler->getLastErrorMessage(), 'UNIQUE_ACRON'))
         $response['error']['message'] = 'El acronimo ' .$updSet['acronym']. ' ya existe';

         if (strpos($this->_dbhandler->getLastErrorMessage(), 'UNIQUE_ALIAS'))
         $response['error']['message'] = 'El alias ' .$updSet['alias']. ' ya existe';
      }

      if ($updated)
           $this->execute('COMMIT');
      else $this->execute('ROLLBACK');

      if ($updated  != -1)
           $status = 'DONE';
      else $status = 'FAIL';

      $response['status']  = $status;
      $response['usuario'] = $this->selectBy(array('id'=> $_POST['pk']));
      $response['usuario'] = $response['flete'][0];

      return $response;
   }

   public function insertUserByPost() {
      $method  = (defined('RM_SCOD_ENC_METHOD'))?RM_SCOD_ENC_METHOD:'sha1';
      $salt    = (defined('RM_SCOD_ENC_SALT'))?RM_SCOD_ENC_SALT:'';
      $sltPass = $salt . $_POST['password'];
      $encPass = hash($method, $sltPass);

      $insSet['id'] = 'NULL';

      foreach($_POST as $fieldname => $fieldvalue) {
         switch($fieldname) {
         case 'firstname': $insSet['first_name'] = $fieldvalue; break;
         case 'lastname' : $insSet['last_name']  = $fieldvalue; break;
         case 'sexo'     : $insSet['sex']        = $fieldvalue; break;
         case 'phone'    : $insSet['phone']      = $fieldvalue; break;
         case 'cellphone': $insSet['cellphone']  = $fieldvalue; break;
         case 'email'    : $insSet['email']      = $fieldvalue; break;
         case 'alias'    : $insSet['alias']      = $fieldvalue; break;
         case 'password' : $insSet['password']   = $encPass; break;
         case 'profile'  : $insSet['profile_id'] = $fieldvalue; break;
         case 'status'   : $insSet['status']     = $fieldvalue; break;
         case 'context'  : $insSet['context']    = $fieldvalue; break;
         case 'acronym'  : $insSet['acronym']    = $fieldvalue; break;
         }
      }

      $insSet['lang_code']     = 'es';
      $insSet['creation_date'] = 'NOW()';
      $insSet['creation_user'] = $_POST['creation_user'];

      $this->execute('BEGIN');

      $added = $this->insertBy($insSet);
      $this->getLastQry();

      if ($added > 0) {
         $userId = $this->getLastId();

         $this->execute('COMMIT');
      }
      else $this->execute('ROLLBACK');


      if ($userId) {
         $response['status'] = 'DONE';
         $response['user']   = $this->selectBy(array('id'=>$userId));
         $response['user']   = $response['flete'][0];
      }
      else {
         $response['status']  = 'FAIL';
         $response['user']    = false;
      }

      return $response;
   }
}
