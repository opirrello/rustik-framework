<?php
class PermissionModel extends Model {
   protected $_primary_key = array('id');

   public function getAll($option=false, $addempty=false) {
      if (!$option)
           $qry = "SELECT id FROM permission";
      else $qry = "SELECT id, description FROM permission";

      $result = $this->execute($qry);
      $newResult = array();

      if ($addempty) {
         if (!$option)
              $newResult[] = '';
         else $newResult[$row['id']] = '';
      }

      if (!empty($result)) {
         foreach ($result as $row) {
            if (!$option)
                 $newResult[] = ($row['id']);
            else $newResult[$row['id']] = $row['description'];
         }
      }

      return $newResult;
   }
}
