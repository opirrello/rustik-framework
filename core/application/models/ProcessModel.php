<?php
class ProcessModel extends Model {
   protected $_primary_key = array('id');

   public function getNotAbleFor($currProf=false, $withDesc=false) {
      if (!$withDesc)
           $sel = array("process.id");
      else $sel = array("process.id", "process.description");

      if ($currProf)
           $where = array('process.id' =>array('NOT IN',
                          "SELECT PRO.id
                             FROM process AS PRO
                            INNER JOIN permission AS PER
                               ON PER.process_id = PRO.id
                              AND PER.profile_id = $currProf"));
      else $where = false;

      $result = $this->selectBy($where, $sel );
      $newResult = array();
      //echo $this->getLastQry();

      if (!empty($result)) {
         foreach ($result as $row) {
            if (!$withDesc)
                 $newResult[] = ($row['id']);
            else $newResult[$row['id']] = $row['description'];
         }
      }

      if (isEmpty($newResult))
           return false;
      else return $newResult;
   }

   public function getProcessTree($inMenu=true) {
      $userInfo = $chckPerm = false;
      $colsArr  = array('id','label','shortname','url','icon','parent_id');
      $filtArr  = ($inMenu) ? array('in_panel'=>'S'):false;

      if (isset($_SESSION['authInfo'])) {
         $userInfo = unserialize($_SESSION['authInfo']);
         $chckPerm = "(SELECT COUNT(*)
                         FROM permission
                        WHERE profile_id = {$userInfo['profile_id']}
                          AND process_id = process.id) AS allowed";

         array_push($colsArr, $chckPerm);
      }

      $result = $this->selectBy($filtArr,
                                $colsArr,
                                array('ORDER BY'=>array('parent_id'=>'ASC','sequence'=>'ASC')));
      //echo $this->_dbhandler->getLastQry();

      if (is_array($result)) {
      foreach($result as $row) {
         $isParent = empty($row['parent_id']);
         $currOpt  = array_shift($row);
         $permOpt  = array_pop($row);
         $prntOpt  = array_pop($row);

         $row['allowed'] = !!$permOpt;

         if ($isParent) { 
            $optArr[$currOpt] = $row;
         }
         else {
            $treeArr = array();
            $this->_getParentTree($currOpt, $result, $treeArr);
            $treeArr = array_reverse($treeArr);

            $str = "\$optArr[" .implode("]['items'][", $treeArr). "]['items'][$currOpt] = \$row;";

            eval($str);

            //$optArr[$prntOpt]['items'][$currOpt] = $row;
         }
      }
      }

      return $optArr;
   }
   
   private function _getParentTree($id, $list, &$zz) {
      foreach($list as $row) {
         if ($row['id'] == $id) {
            if (!empty($row['parent_id'])) {
               $zz[] = $row['parent_id'];

               $this->_getParentTree($row['parent_id'], $list, $zz); 
               break;
            }
         }
      }
   }
}
