<?php
class ProfileModel extends Model {
   protected $_primary_key = array('id');

   public function getAll($option=false, $addempty=false) {
      if (!$option)
           $qry = "SELECT id FROM profile";
      else $qry = "SELECT id, description FROM profile";

      $result = $this->execute($qry);
      $newResult = array();

      if ($addempty) {
         if (!$option)
              $newResult[] = '';
         else $newResult[$row['id']] = '';
      }

      if (!empty($result)) {
         foreach ($result as $row) {
            if (!$option)
                 $newResult[] = ($row['id']);
            else $newResult[$row['id']] = $row['description'];
         }
      }

      return $newResult;
   }

   public function selectProfileByPost() {
      $profQry  = "SELECT PROF.*, (SELECT COUNT(*) FROM permission WHERE profile_id = PROF.id) AS cant_procesos
                     FROM profile AS PROF 
                    WHERE PROF.id = ".$_POST['pk'];
      $profData = $this->execute($profQry);
                       
      if (!isEmpty($profData)) {
         $profData = $profData[0];
         $procData = $this->execute("SELECT PROC.id, PROC.description, PERM.auditable
                                       FROM permission PERM 
                                      INNER JOIN process AS PROC
                                         ON PERM.process_id = PROC.id
                                        AND PERM.profile_id = ". $_POST['pk'] ."
                                      ORDER BY PROC.description ASC");
         $profData['procesos'] = $procData;
      }

      $response['status'] = (!isEmpty($profData)) ? 'DONE':'FAIL';
      $response['extra']  = $profData;

      return $response;
   }

   public function deleteProfileByPost() {
      $this->execute('BEGIN');

      $response['perfil'] = $this->selectBy(array('id'=> $_POST['pk']));
      $response['perfil'] = $response['perfil'][0];
      
      $deleted = $this->deleteBy(array('id'=>$_POST['pk']));

      if ($deleted != -1) {
         $permModel = new PermissionModel();
         $deleted = $permModel->deleteBy(array('profile_id'=>$_POST['pk']));

         if ($deleted != -1)
              $this->execute('COMMIT');
         else $this->execute('ROLLBACK');
      }
      else $this->execute('ROLLBACK');

      $status = ($deleted != -1) ? 'DONE':'FAIL';

      $response['status'] = $status;
      return $response;
   }

   public function updateProfileByPost() {
      foreach($_POST as $fieldname => $fieldvalue) {
         switch($fieldname) {
         case 'shortname'   : $updSet['shortname']   = $fieldvalue; break;
         case 'description' : $updSet['description'] = $fieldvalue; break;
         case 'labelcolor'  : $updSet['labelcolor']  = strtoupper($fieldvalue); break;
         }
      }

      $updSet['update_date'] = 'NOW()';
      $updSet['update_user'] = $_POST['update_user'];

      $permModel = new PermissionModel();
      $perm4Proc = $permModel->selectBy(array('profile_id'=>$_POST['pk']), array('process_id'));
      $perm4Proc = (isEmpty($perm4Proc)) ? [] : array_column($perm4Proc, 'process_id');
      //echo $permModel->getLastQry();

      $this->execute('BEGIN');

      $updated = $this->updateBy($updSet, array($_POST['pk']));
      //echo $this->getLastQry();

      ////// INSERTAR/ELIMINAR PERMISOS DE PERFIL MODIFICADO ///
      if ($updated) {
         $perm2Insert = [];

         if (!isEmpty($_POST['permission-data'])) {
            foreach($_POST['permission-data'] as $procId) {
               if (!in_array($procId, $perm4Proc))
               $perm2Insert[] = $procId;
            }

            $perm2Delete = array_values(array_diff($perm4Proc, $_POST['permission-data']));

            //------------ Adding new process added to edited profile -------------------//
            if (!isEmpty($perm2Insert)) {
               foreach($perm2Insert as $procId) {
                  $insSet                  = [];
                  $insSet['id']            = 'NULL';
                  $insSet['profile_id']    = $_POST['pk'];
                  $insSet['process_id']    = $procId;
                  $insSet['description']   = '';
                  $insSet['auditable']     = 'S';
                  $insSet['creation_date'] = 'NOW()';
                  $insSet['creation_user'] = $_POST['update_user'];
                  $insSet['update_date']   = 'NULL';
                  $insSet['update_user']   = 'NULL';

                  $insGroup[] = $insSet; 
               }

               $added = $permModel->insertBy($insGroup);
               //echo $permModel->getLastQry();

               if ($added <=0)
               $permInsFail = true;
            }

            //------------ Removing old process existent into edited profile -----------//
            if (!isEmpty($perm2Delete)) {
               $deleted = $permModel->deleteBy(array('process_id'=>$perm2Delete, 'profile_id'=>$_POST['pk']));
               //echo $permModel->getLastQry();

               if ($deleted <=0)
               $permUpdFail = true;
            }

            //---- IF ANY PROCESS COULD BE ADDED OR DELETED, ROLLBACK ----//
            if ($permInsFail || $permUpdFail) {
               $this->execute('ROLLBACK');
               $status = 'FAIL';
            }
            else {
               $this->execute('COMMIT');
               $status = 'DONE';
            }
         }
         //---- IF ANY PROCESS NEED TO BE ADDED/DELETED, COMMIT ----//
         else {
            $this->execute('COMMIT');
            $status = 'DONE';
         }
      }
      //---- IF CURRENT PROFILE COULDN'T BE EDITED, ROLLBACK ----//
      else {
         $this->execute('ROLLBACK');
         $status = 'FAIL';
      }

      //===== build response structure =====//
      $response['status'] = $status;
      $response['perfil'] = $this->selectBy(array('id'=> $_POST['pk']));
      $response['perfil'] = $response['perfil'][0];

      return $response;
   }

   public function insertProfileByPost() {
      $insSet['id'] = 'NULL';

      foreach($_POST as $fieldname => $fieldvalue) {
         switch($fieldname) {
         case 'shortname'   : $insSet['shortname']   = $fieldvalue; break;
         case 'description' : $insSet['description'] = $fieldvalue; break;
         case 'labelcolor'  : $insSet['labelcolor']  = $fieldvalue; break;
         }
      }

      $insSet['creation_date'] = 'NOW()';
      $insSet['creation_user'] = $_POST['creation_user'];

      $this->execute('BEGIN');

      $added = $this->insertBy($insSet);
      //echo $this->getLastQry();
      
      ////// INSERTAR PERMISOS ///
      if ($added > 0) {
         $profId = $this->getLastId();

         if (!isEmpty($_POST['permission-data'])) {
            foreach($_POST['permission-data'] as $procId) {
               $insSet                  = [];
               $insSet['id']            = 'NULL';
               $insSet['creation_date'] = 'NOW()';
               $insSet['creation_user'] = $_POST['creation_user'];
               $insSet['profile_id']    = $profId;
               $insSet['process_id']    = $procId;
               $insSet['description']   = '';
               $insSet['auditable']     = 'S';

               $_POST['permisos'] = $insSet; 
            }

            $permModel = new PermissionModel();
            $added     = $permModel->insertBy($_POST['permisos']);
            $permId    = ($added > 0) ? $permModel->getLastId():false;

            if ($added > 0)
                 $this->execute('COMMIT');
            else $this->execute('ROLLBACK');
         }
         else $this->execute('ROLLBACK');
      }
      else $this->execute('ROLLBACK');

      if ($profId && $permId) {
         $response['status'] = 'DONE';
         $response['perfil'] = $this->selectBy(array('id'=>$profId));
         $response['perfil'] = $response['perfil'][0];
      }
      else {
         $response['status']  = 'FAIL';
         $response['perfil']  = false;
      }

      return $response;
   }

   public function getProfileForListing($filterArr=false) {
      $query   = rightEncoding($_POST['query']);
      $orderby = (empty($_POST['orderby']))? PROF_LISTING_ORDERBY : $_POST['orderby'];
      $orient  = (empty($_POST['orient'])) ? PROF_LISTING_ORDERDIR: strtoupper($_POST['orient']);
      $page    = (empty($_POST['page']))   ? RF_PAGN_INITPAGE     : $_POST['page'];

      //----------- setting orderby ----------------------------------//
      switch($orderby) {
      case "shortname"   : $orderFields = "shortname";   break;
      case "description" : $orderFields = "description"; break;
      default            : $orderFields = $orderby;      break;
      }

      $orderFields = explode(':', $orderFields);

      foreach ($orderFields as $field) {
         $order[] = "$field $orient";
      }

      $order = implode(', ', $order);


      //----------- setting filtering -------------------------------//
      list($filtCond, $filtArr) = $this->_setFilterCond($filterArr, 'explicit');

      if ($filtCond && is_array($filtCond))
           $where = " WHERE ". implode(' AND ', $filtCond);
      else $where = false;

      //----------- setting SELECT ----------------------------------//
      $cols = "id, description, shortname, labelcolor, (SELECT COUNT(*) FROM permission WHERE profile_id = PROF.id) AS cant_procesos";
      $from = "FROM profile AS PROF";
      $order= "ORDER BY $order";

      //echo "SELECT $cols $from $where $order";
      $results = $this->execute("SELECT $cols $from $where $order", $filtArr, $page);
      $results['query'] = $this->_dbhandler->getLastQry();
      $results['ordering'] = array('orderby'=>$orderby, 'orient'=>$orient);
      return $results;
   }


   private function _setFilterCond($filterArr=false, $responseType='structure') {
      //----------- setting filtering -------------------------------//
      $filtArr = Array();
      $filtCond= Array();
      $filtStr = Array();

      if ($filterArr) {
         foreach($filterArr as $filter=>$value) {
            switch ($filter) {
            /// not used right now = let it here for future implementation
            case 'showrows':
               $page = (empty($filterArr['showrows'])) ? $page : array(0=>$filterArr['showrows'],2=>$page);
               break;

            case 'searchmaestro':
               $filtArr[]  = "%$value%";
               $filtCond[] = "description LIKE ?";
               $filtStr['description'] = array('LIKE', $value);
               break;
            }
         }
      }

      if ($responseType == 'explicit')
           return array($filtCond, $filtArr);
      else return $filtStr;
   }
}
