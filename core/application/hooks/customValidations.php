<?php
function customValidations($rule, $checkval, $posted) {
   $isValid = true;
   
   switch ($rule) {
   case 'url' :
        if ($checkval && !empty($posted)) {
           if (!checkValidUrl($posted))
           $isValid = false;
        }
        break;      
   }

   return $isValid;
}
