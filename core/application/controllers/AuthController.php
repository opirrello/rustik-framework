<?php
class AuthController extends PageController {
   protected $_avoid_set_template = true;
   protected $_avoid_header = false;

   public function __construct() {
      parent::__construct();
   }
   
   public function showLogin() {
      global $g_profileInfo;

      if (SecurityHandler::getInstance()->sessionExists()) {
         SecurityHandler::getInstance()->startSession();
         $authInfo = SecurityHandler::getInstance()->getFromSession('authInfo'); 
         $landingPage = SecurityHandler::getInstance()->getFromSession('authReferrerPage');
      }

      if (!empty($authInfo)) {
         $authInfo = unserialize($authInfo);
         $go2Page  = $this->_getLandingFor($authInfo['shortname']);

         header('location:'.RF_DOMAIN.$go2Page);
      }
      else {
         $this->_tplHandler->addJsFiles('custom/handle.user.js');
         $this->_tplHandler->useTemplate('login');

         $jsonform = FormHandler::getInstance()->getJSONForm('login', array('landing'=>$landingPage));
         $this->_tplHandler->addJsScript('var loginJson = '.$jsonform.';'); 
      }
   }

   public function closeSession() {
      if (SecurityHandler::getInstance()->sessionExists()) {
         SecurityHandler::getInstance()->startSession();

         $authInfo = SecurityHandler::getInstance()->getFromSession('authInfo');
         $authInfo = unserialize($authInfo);

         SecurityHandler::getInstance()->destroySession();

         $this->_differWith($authInfo['id']);

         if (empty($_SESSION))
              ErrorHandler::getInstance()->sendErrorHeader('session-closed');
         else ErrorHandler::getInstance()->sendErrorHeader('session-problem');
      }
      else ErrorHandler::getInstance()->sendErrorHeader('session-problem');
   }

   public function startSession() {
      global $g_profileInfo;

      $protocol  = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
      $postData  = sanitizeRequest();
      $userModel = new UserModel();

      if ($userModel->isActiveUser($postData['user'], $postData['pass'])){
         $userInfo = $userModel->getBasicData($postData['user']);
         $userInfo = $userInfo[0];
         $userInfo = $this->buildAuthInfo($userInfo);
         $go2Page  = $this->_getLandingFor($userInfo['shortname']);
         $go2Page  = (empty($postData['landing'])) ? $go2Page:$postData['landing']; 

         SecurityHandler::getInstance()->startSession();
         SecurityHandler::getInstance()->registerInSession('authInfo', serialize($userInfo));
         SecurityHandler::getInstance()->registerInSession('authLastActivity', time());
         SecurityHandler::getInstance()->registerInKeepSession($userInfo['id']);

         $this->_differWith($userInfo['id']);

         logEvent("200 - User ".$userInfo['alias']." trying to login : DONE",  $userInfo);

         ErrorHandler::getInstance()->sendErrorHeader('session-started');
         echo json_encode(array('toCallBack'=>array('alias'=>$userInfo['alias'], 'redirect'=>$go2Page, 'options'=>$sess_opts)));
      }
      else {
         ErrorHandler::getInstance()->sendErrorHeader('user-failed');
         $errorArr = ErrorHandler::getInstance()->getErrorData('user-failed');
         echo json_encode(array('message'=>array('title'=>$errorArr['title'],
                                'description'=>$errorArr['description'],'mode'=>'fixed'),
                                'toCallBack'=>array('status'=>$errorArr['httpCode']) ));
      }
   }

   protected function _differWith($data) {
      $salt = (defined('RM_AUTH_ENC_SALT'))?RM_AUTH_ENC_SALT:'';
      $hash = crc32(serialize($salt.$data.$salt));
      time_nanosleep(0, abs($hash % 100000));
   }

   protected function _getLandingFor($currProfile) {
      if (defined('PROFILE_LANDING')) {
         $landingsByProf = multiExplode(array('|',':'), PROFILE_LANDING);

         foreach($landingsByProf as $landingData) {
            list($profileCode, $landingPage) = $landingData;

            if ($profileCode == $currProfile) {
               break;
            }
         }
      }
      else if (defined('DEFAULT_LANDING')) {
         $landingPage = DEFAULT_LANDING;
      }
      else $landingPage = '';

      return $landingPage;
   }

   public function buildAuthInfo($basicData) {
      $authInfo = $basicData;
      $authInfo['first_name'] = rightEncoding($basicData['first_name']);
      $authInfo['last_name']  = rightEncoding($basicData['last_name']);

      $accIds   = explode(':', $basicData['account_ids']);
      $authInfo['account_ids'] = array('id' =>$accIds[0], 'cpa_id' => $accIds[1], 'crm_id' => $accIds[2]);

      return $authInfo;
   }
}
