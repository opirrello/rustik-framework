<?php
class PageController extends Controller {
   protected $_tplHandler;
   protected $_langHandler;

   public function __construct() {
      parent::__construct();

      $this->_tplHandler = TemplateHandler::getInstance();
      $this->_langHandler= LanguageHandler::getInstance();
      
      if (!isset($this->_avoid_set_template) || !$this->_avoid_set_template) {
         if (get_class($this) != 'PageController') {
            $className    = get_class($this);
            echo $templateName = strtolower(substr($className, 0, strpos($className,'Controller')));

            if (SystemHandler::getInstance()->isDefinedView($templateName)) {
               $this->_tplHandler->useTemplate($templateName);
            } else ErrorHandler::getInstance()->showError('not-found');
         }
      }

      $this->_handleAuthentication();
      //$this->_handleInternationalization();
   }

   protected function _handleAuthentication() {
      $authInfo = $tokenInfo = false;

      if ($this->_currentPageUnderAuth()) {
         ////// RECOVER REMEMBER-ME SESSION IF EXISTS AND CHECK IF IT'S VALID //////
         $keepData   = SecurityHandler::getInstance()->validateKeepSession();
         $keepStatus = $keepData['status'];
         $keepInfo   = $keepData['info'];
   
         /// If keep session is invalid / disabled / incorrect ... show error and stop process
         if ($keepStatus == -1) {
            ErrorHandler::getInstance()->showError('security-alert-keep-differ');
         }
         elseif ($keepStatus == -2) {
            ErrorHandler::getInstance()->showError('security-alert-keep-missing');
         }
   
         ////// IF THERE IS AN ACTIVE SESSION //////
         if ( SecurityHandler::getInstance()->sessionExists() ) {
            SecurityHandler::getInstance()->startSession();
   
            $authInfo = SecurityHandler::getInstance()->getFromSession('authInfo');
            $authInfo = unserialize($authInfo);
            
   
            switch($keepStatus) {
               // If keep session is in use and is valid, restore it (new token asigned)
               // and restore session life time
               case 1:
                  SecurityHandler::getInstance()->restoreKeepSession();
                  SecurityHandler::getInstance()->registerInSession('authLastActivity', time());
                  break;
            
               // If Keep cookie unset or Keep session is disabled, only must check normal session
               case 0:
               case 2:
                  //---- Checking inactivity elapsed time (if there isn't a keep session) -------//
                  $lastActivity= SecurityHandler::getInstance()->getFromSession('authLastActivity');
                  $elapsedTime = time() - $lastActivity; // elapsed time in seconds
   
                  // If allowed inactivity time was exceed, current session is destroyed   
                  // If not, current session remains and last activity is updated
                  
                  if ($elapsedTime > (RF_AUTH_MAXINACT * 60)) {
                     $authInfo = false;
                     SecurityHandler::getInstance()->destroySession();
                  }
                  else SecurityHandler::getInstance()->registerInSession('authLastActivity', time());
                  
                  break;
            }
         }
         ///// IF THERE IS NOT ANY DEFINED SESSION ///// 
         else {
            /** Checking if 'remember me' cookie was set to auto-log 'remembered' user **/
            if (is_array($keepInfo) && !empty($keepInfo)) {
               $authInfo = $usuModel->getBasicData($keepInfo['id_usuario']);
               $authInfo = $authInfo[0];
               
               SecurityHandler::getInstance()->startSession();
               SecurityHandler::getInstance()->registerInSession('authInfo', serialize($authInfo));
               SecurityHandler::getInstance()->registerInSession('authLastActivity', time());
               $authInfo = $usuInfo;
            }
         }
   
         ///// if user is logged in, then current header is displayed
         if (!empty($authInfo) && is_array($authInfo)) {
            $this->_tplHandler->loadSnippetInto('snpt_header_logged_in', 'header');
            $this->_showShortcutsFor($authInfo);
         }
         ///// if not, then is redirected to authentication page
         else {
            $jsonRequest = strpos($_SERVER['HTTP_ACCEPT'], 'json') !== false;

            if ($jsonRequest) {
               $sessionInfo['redirect'] = RF_AUTH_LOGINURL;

               ErrorHandler::getInstance()->sendErrorHeader('session-closed');
               echo json_encode($sessionInfo);
               exit;
            }
            else {
               //SecurityHandler::getInstance()->destroySession();
               SecurityHandler::getInstance()->startSession();
               SecurityHandler::getInstance()->registerInSession('authReferrerPage', $_SERVER['REQUEST_URI']);
               header('Location: '.RF_AUTH_LOGINURL);
               exit;
            }
         }
      ///// if current page dont need authentication, then single header is render (if it's neccesary) & continue
      }
      else {
         if (!$this->_avoid_header)
         $this->_tplHandler->loadSnippetInto('snpt_header_logged_out', 'header');
      }
   }

   protected function _showShortcutsFor($authInfo) {
      $this->_tplHandler->setVariable('user_name'   , $authInfo['first_name'].' '.$authInfo['last_name']);
      $this->_tplHandler->setVariable('user_profile', $authInfo['shortname']);
      $this->_tplHandler->setVariable('user_thumb'  , getUserThumbPath($authInfo['id']));
   }

   protected function _currentPageUnderAuth() {
      $url_mapping = RouterHandler::getInstance()->getMapping();
      $currPage    = $_SERVER['REQUEST_URI'];
      $underAuth   = true;

      foreach ($url_mapping as $mappingPos => $mappingData) {
         $mappingPattern = '\/'.$mappingData['pattern'];

         preg_match('/^'.$mappingPattern.'$/', $currPage, $currMatch);

         if (!isEmpty($currMatch) && (isEmpty($mappingData['auth']) || $mappingData['auth'] === false)) {
            $underAuth = false;
            break;
         }
      }

      return $underAuth;
   }

   protected function _handleInternationalization() {
      $authInfo = SecurityHandler::getInstance()->getFromSession('authInfo');
      $authInfo = unserialize($authInfo);
      $langInfo = translate_info();

      $langModel = new LanguageModel();
      $langArr   = $langModel->selectBy();

      if (!isEmpty($langArr)) {
         foreach($langArr as $langData) {
            if ($langData['code'] == $langInfo['curr']) {
               $this->_tplHandler->setVariable('language_curr_flag', $langData['code']);
               $this->_tplHandler->setVariable('language_curr_name', $langData['display']);
            }
            else {
               $this->_tplHandler->setVariable('language_code', $langData['code']);
               $this->_tplHandler->setVariable('language_name', $langData['display']);
               $this->_tplHandler->appendBlock('languages');
            }
         }
      }
   }

   protected function _getParentOf($currProc, $procArr, $parentProc=false) {
      $fatherItem = false;

      //echo "<BR>---> analysing $currProc is child of $parentProc";

      foreach ($procArr as $procInfo) {
         //echo "<BR>-------> compare $currProc against ". $procInfo['shortname'];
         if ($currProc == $procInfo['shortname']) {
            //echo " [ FOUNDED ]";
            $fatherItem = $parentProc;
            break;
         }
         elseif (isset($procInfo['items'])) {
            //echo " [ HAS CHILDS ]";
            $fatherItem = $this->_getParentOf($currProc, $procInfo['items'], $procInfo['shortname']);

            if ($fatherItem)
            break;   
         }
      }

      return $fatherItem;
   }

   public function showMainMenu($currProc=false) {
      $userModel  = new UserModel();
      $procModel  = new ProcessModel();

      $allProcArr = $procModel->getProcessTree(false);
      $inMenuArr  = $procModel->getProcessTree();
      $parentCode = $this->_getParentOf($currProc, $allProcArr);

      if (!$parentCode)
      $parentCode = $currProc;

      //--------------- rendering shortcuts ----------------//
      if (defined('OPTIONS_SHORTCUT') && !empty('OPTIONS_SHORTCUT')) {
         $shortArr = explode(':', OPTIONS_SHORTCUT);

         foreach($shortArr as $procCode) {
            if ($userModel->hasPermissionFor($procCode)) {
               $this->_tplHandler->writeBlock(strtolower($procCode));
            }
         }
      }
      
      //--------------- rendering main Menu ----------------//
      foreach ($inMenuArr as $procInfo) {
         $optClasses = array();
         $itemAttributes = $attrList = array();
         $itemCaret = false;

         if ($userModel->hasPermissionFor($procInfo['shortname'])) {
            //echo "<BR>$currProc // ". $procInfo['shortname'];

            if ($parentCode == $procInfo['shortname']) {
               $optClasses[] = 'active';

               if (!empty($procInfo['url']))
               $this->_tplHandler->exposeConstant('DEFAULTPAGE', $procInfo['url']);
            }

            if (!empty($procInfo['items'])) {
               $optHref = '#';
               $optClasses[] = 'dropdown';
               $itemAttributes['class'] = array('dropdown-toggle');
               $itemAttributes['data-toggle'] = array('dropdown');

               $caretObj = HtmlHandler::createElement('span');
               $caretObj->setAttribute('class', 'caret');
               $itemCaret = $caretObj->getHTML();

               $this->_tplHandler->emptyRenderedBlock('items');

               foreach ($procInfo['items'] as $itemId => $itemInfo) {
                  if ($currProc == $itemInfo['shortname']) {
                     $itemUrl = '';
                     $itemCss = ' class="active"';
                     $itemTxt = $itemInfo['label'];
                  }
                  else {
                     $itemUrl = ' href="'.$itemInfo['url'].'"';
                     $itemCss = '';
                     $itemTxt = $itemInfo['label'];
                  }

                  $this->_tplHandler->setVariable('item_href', $itemUrl);
                  $this->_tplHandler->setVariable('item_text', $itemTxt);
                  $this->_tplHandler->setVariable('item_curr', $itemCss);
                  $this->_tplHandler->appendBlock('items');
               }

               foreach($itemAttributes as $attr => $values) {
                  $attrList[] = "$attr = \"". implode(' ', $values) ."\"";
               }
            }
            else {
               $optHref = $procInfo['url'];
            }

            $this->_tplHandler->setVariable('option_css', ((!empty($optClasses)) ? 'class="'.implode(' ',$optClasses).'"' : '') );
            $this->_tplHandler->setVariable('option_label_href', $optHref );
            $this->_tplHandler->setVariable('option_label_attr', implode(' ',$attrList) );
            $this->_tplHandler->setVariable('option_label_text', $procInfo['label'] );
            $this->_tplHandler->setVariable('option_label_caret',$itemCaret );
            $this->_tplHandler->appendBlock('options');
         }
      }
   }

   protected function _showPaginationDetail($pagnData, $jsonRequest=false) {
      $toPage  = ($pagnData['curr_page'] * $pagnData['rows_per_page']);
      $fromPage= $toPage - $pagnData['rows_per_page'] + 1;
      $toPage  = ($pagnData['num_rows'] < $toPage) ? $pagnData['num_rows'] : $toPage;
      $detail  = MessengerHandler::getInstance()->getMessage('listing-text', array($fromPage, $toPage, $pagnData['num_rows'], 'registros'));

      if (!$jsonRequest)
           $this->_tplHandler->setVariable('pagn_detail', $detail);
      else return $detail;
   }

   protected function _showColumns($orderData, $jsonRequest=false) {
      // displaying ordering and orientation data //
      
      // First, walking each tab options to display if is active and if is not currently filtered by this
      foreach($this->_listingColumns as $colname => $colInfo) {
         if (!$colInfo['active'])
         continue;

         $coldesc = strtolower($colInfo['desc']);

         if (!empty($orderData['orderby']) && $orderData['orderby'] == $colInfo['code']) {
            $orient_curr = strtolower($orderData['orient']);
            $orient_hay  = true;
         }
         else {
            $orient_curr = strtolower(RF_PAGN_ORDDIR);
            $orient_hay  = false;
         }

         $orient_prv = ($orient_curr == "asc") ? "desc":"asc";
         $orient_tit = ($orient_curr == "asc") ? "Ordenar por $coldesc ascendente" : "Ordenar por $coldesc descendente";

         if (!$jsonRequest) {
            $this->_tplHandler->setVariable('head_col_ori_tit', $orient_tit);
            $this->_tplHandler->setVariable('head_col_ori_css', $orient_curr);
            $this->_tplHandler->setVariable('head_col_ori_sel', ($orient_hay) ? 'selected' : '');

            $this->_tplHandler->setVariable('head_col_name', $colInfo['desc']);
            $this->_tplHandler->setVariable('head_col_code', $colInfo['code']);
            $this->_tplHandler->setVariable('head_col_ornw', $orient_curr);

            $this->_tplHandler->appendBlock('results_head_columns');
         }
         else {
            $columnSet[$colname]['aux']    = $orderData['orderby'] . ' / ' . $colInfo['code'];
            $columnSet[$colname]['code']   = $colInfo['code'];
            $columnSet[$colname]['name']   = $colInfo['desc'];
            $columnSet[$colname]['orderby']= ($orient_hay) ? true : false;
            $columnSet[$colname]['orient'] = $orient_curr;
            $columnSet[$colname]['orientp']= $orient_prv;
         }
      }

      if ($jsonRequest)
      return $columnSet;
   }

   protected function _setListingUsing($listingParameters) {
   }

   protected function _showListingUsing($dataSet, $jsonRequest=false, $jsonParams=false) {
      $this->_tplHandler->addJsFiles('custom/pagination.js');
      
      $userModel   = new UserModel();
      $cotCheckeds = ($_POST['checkeds'])?explode(':', $_POST['checkeds']):array();

      // displaying pagination & ordering data //
      if (!$jsonRequest) {
         $this->_showColumns($dataSet['ordering']);
         $this->_showPaginationDetail($dataSet['pagination']);
         $this->_tplHandler->setPagination($dataSet['pagination'], $dataSet['ordering']);
      }
      else {
         $results['columnset']  = $this->_showColumns($dataSet['ordering'], true);
         $results['pagingdet']  = $this->_showPaginationDetail($dataSet['pagination'], true);
         $results['pagination'] = $dataSet['pagination'];
         $results['ordering']   = $dataSet['ordering'];
         $results['extra']      = $jsonParams;
         //$results['query']   = $dataSet['query'];
      }         
         
      //------------------------------------ BEGIN mostrando datos ----------------------------------//
      $customFields = (isset($dataSet['customize'])) ? array_keys($dataSet['customize']) : [];
      $idx          = 0;

      foreach($dataSet['rows'] as $rowInfo) {
         $fieldArr = array_keys($this->_listingColumns);
         $idx++; // used below to define odd/even row setting

         //--------- setting row checked to apply some action ----------
         if (in_array($rowInfo['id'], $cotCheckeds)) 
              $rowInfo['checked'] = true;
         else $rowInfo['checked'] = false;

         if (!$jsonRequest)
         $this->_tplHandler->emptyRenderedBlock('results_body_columns');

         //--------- Process each row ----------//
         foreach($fieldArr as $fieldname) {
            if (!$this->_listingColumns[$fieldname]['active'])
            continue;

            //--------- special fields renderization ---------
            if (in_array($fieldname, $customFields) && !isEmpty($rowInfo[$fieldname])) {
               $rowInfo[$fieldname."_ori"] = $rowInfo[$fieldname];

               if (!$jsonRequest)
                    $rowInfo[$fieldname] = $this->_customizeField($fieldname, $dataSet['customize'][$fieldname], $rowInfo);
               else $rowInfo[$fieldname] = array('html'=>$this->_customizeField($fieldname, $dataSet['customize'][$fieldname], $rowInfo));
            }
                  
            //--------- printing field value -----------------
            if (!$jsonRequest) {
               $attrs   = false;
               $classes = false;

               //************** adding extra CSS classes *********
               if (array_key_exists('classes', $this->_listingColumns[$fieldname]) &&
                   is_array($this->_listingColumns[$fieldname]['classes'])) {

                   $classes = implode(' ', $this->_listingColumns[$fieldname]['classes']);
               }

               //************** adding extra HTML attributes *********
               if (array_key_exists('attributes', $this->_listingColumns[$fieldname]) &&
                   is_array($this->_listingColumns[$fieldname]['attributes'])) {

                   $attrs = [];

                   foreach($this->_listingColumns[$fieldname]['attributes'] as $attrName=>$attrValue) {
                      $attrs[] = "$attrName=\"$attrValue\"";
                   }

                   $attrs = implode(' ', $attrs);
               }

               $this->_tplHandler->setVariable('body_col_content'    , $rowInfo[$fieldname]);
               $this->_tplHandler->setVariable('body_col_attributes' , $attrs);
               $this->_tplHandler->setVariable('body_col_css'        , $classes);
               $this->_tplHandler->appendBlock('results_body_columns');
            }
         }

         //--------- acciones por fila ---------//
               
         //-- empty action block if is not json request
         if (!$jsonRequest)
         $this->_tplHandler->emptyRenderedBlock('results_body_actions');

         foreach($this->_listingActions as $actId=>$actInfo) {
            //---- avoid render action that is not row level ----//
            if (isset($actInfo['level']) && !in_array('row', $actInfo['level']))
            continue;

            //---- compile status that let some actions out ----//
            // deniedTo:
            //    conditions: (array)
            //       content  : - array (array with fieldname and value as positions)
            //                  - function (return true or false) 
            //
            //    all conditions must be true to avoid the action              
            //    content that is not array can has the oiperator '!' as prefix to change value check for 'is not'
            //
            //
            //----------- future implementation -----------------------
            //
            //array('oper'=>'or',  ------> condition group to control logical operator (OR or AND)
            //      'content'=>array(
            //          array('field'=>'status_ori', 'values'=>array('VEND','PERD'),
            //          $function)
            //),
            //array('field'=>'status_ori', 'values'=>array('VEND','PERD'),
            //array('field'=>'status_ori', 'values'=>array('VEND','PERD')
            //

            $denied    = false;
            $actionCss = $actInfo['stylename'];
            $actionRel = $rowInfo['id'];

            if (isset($actInfo['deniedTo']) && is_array($actInfo['deniedTo'])) {

               foreach($actInfo['deniedTo'] as $condData) {
                  if (is_array($condData)) {
                     $avoidArr = (empty($condData))?array():$condData;
                     $extraArr = Array();
   
                     if (!isEmpty($avoidArr)) {
                        $avoid_field  = $avoidArr['field'];
                        $avoid_values = $avoidArr['values'];
                        $avoid_field  = ($avoid_field[0] == '!') ? substr($avoidArr['field'],1) : $avoidArr['field'];

                        //var_dump($rowInfo[$avoid_field]);
                        //var_dump($avoid_values);
                        if ($avoidArr['field'][0] == '!')
                             $denied = !in_array($rowInfo[$avoid_field], $avoid_values);
                        else $denied = in_array($rowInfo[$avoid_field], $avoid_values);
                     }
                  }
                  elseif (is_callable($condData)) {
                     $denied = $condData($rowInfo);
                  }

                  if ($denied) break;
               }
            }

            if ( !$denied && $userModel->hasPermissionFor($actInfo['prcode']) ) {
               $actionOmit = '';

               if (array_key_exists('extra',$actInfo)) {
               foreach($actInfo['extra'] as $attrName=>$attrValue) { $extraArr[] = "$attrName = \"$attrValue\""; }
                  $actionExtra = implode(' ',$extraArr);
               }
            }
            else {
               $actionOmit  = ' disabled';
               $actionExtra = '';
            }

            if (!$jsonRequest) {
               $this->_tplHandler->setVariable('action_rel',  $actionRel);
               $this->_tplHandler->setVariable('action_name', $actId);
               $this->_tplHandler->setVariable('action_omit', $actionOmit);
               $this->_tplHandler->setVariable('action_css',  $actionCss);
               $this->_tplHandler->setVariable('action_desc', $actInfo['desc']);
               $this->_tplHandler->setVariable('action_extra',$actionExtra);

               $this->_tplHandler->appendBlock('results_body_actions');
            }
            else {
               if (array_key_exists('extra',$actInfo))
                    $actionExtra = $actInfo['extra'];
               else $actionExtra = false;

               $actionArr['name'] = $actId;
               $actionArr['desc'] = $actInfo['desc'];
               $actionArr['rel']  = $actionRel;
               $actionArr['omit'] = $actionOmit;
               $actionArr['css']  = trim("$actionCss $actionOmit");
               $actionArr['extra']= $actionExtra;

               $rowInfo['actions'][$actId] = $actionArr;
            }
         }

         //--------- otros renderizados por fila ----------
         $rowProps = [];

         if (!isEmpty($dataSet['customrow'])) {
            foreach($dataSet['customrow'] as $flagHTML => $flagContent) {
               switch($flagHTML) {
               case 'class':
                  if (is_callable($flagContent))
                       $rowClass = $flagContent($rowInfo);
                  else $rowClass = $flagContent;
                  break;

               default:
                  if (is_callable($flagContent))
                       $tagProps[$flagHTML] = $flagContent($rowInfo); 
                  else $tagProps[$flagHTML] = $flagContent;
               }
            }

            if (!isEmpty($tagProps)) {
               foreach($tagProps as $prop => $value) {
                  $rowProps[] = "$prop=\"$value\"";
               }
            }
         }

         //--------- seteo final y append de fila ---------//
         if (!$jsonRequest) {
            $this->_tplHandler->setVariable('body_col_id'   ,  $rowInfo['id']);
            $this->_tplHandler->setVariable('body_row_odd'  , (($idx % 2) ? 'odd':'even'));
            $this->_tplHandler->setVariable('body_row_css'  , (!isEmpty($rowClass)) ? ' '.$rowClass : '');
            $this->_tplHandler->setVariable('body_row_props', (!isEmpty($rowProps)) ? explode(' ',$rowprops) : '');
            $this->_tplHandler->appendBlock('results_body_rows');
         }            
         else {
            $rowInfo['row_odd']   = (($idx % 2) ? 'odd':'even');
            $rowInfo['row_css']   = (!isEmpty($rowClass)) ? $rowClass : false;
            $rowInfo['row_props'] = (!isEmpty($rowProps)) ? $rowProps : false;
            $results['results'][] = $rowInfo;
         }
      }

      //--------- Si es un request JSON se devuelve el json via AJAX ---------//
      if ($jsonRequest)
      return json_encode($results);
   }

   private function _customizeField($fieldName, $fieldDesign, $rowInfo) {
      if (isset($fieldDesign['name']))
           $htmlName = $fieldDesign['name'];
      else $htmlName = $fieldName;
      
      if (isset($fieldDesign['id']))
           $htmlId = $fieldDesign['id'];
      else $htmlId = $fieldName;
      
      if (isset($fieldDesign['reference']))
           $htmlVal = $rowInfo[$fieldDesign['reference']];
      else $htmlVal = $rowInfo['id'];
      

      switch ($fieldDesign['component']) {
         case 'label':
            $spanObj   = HTMLHandler::createElement('span');
            $spanObj->setAttribute('class', $fieldDesign['styles']['css']); // 'select2-filter estados');
            $spanObj->setAttribute('style', $fieldDesign['styles']['style']); // 'select2-filter estados');
            $spanObj->setAttribute('id'   , $htmlId.$rowInfo['id']); //'single_acctrn'.$rowInfo['id']);
            $spanObj->addContent($rowInfo[$fieldName]);

            return $spanObj->getHtml();
            break;

         case 'dropdown':
            $spanObj   = HTMLHandler::createElement('span');
            $selectObj = HTMLHandler::createElement('select');

            $spanObj->setAttribute('class', $fieldDesign['styles']['main']); // 'select2-filter estados');

            $selectObj->setAttribute('class', $fieldDesign['styles']['list']); //'sp-select single-acctrn');
            $selectObj->setAttribute('name' , $htmlName.$rowInfo['id']); //'single_acctrn'.$rowInfo['id']);
            $selectObj->setAttribute('id'   , $htmlId  .$rowInfo['id']); //'single-acctrn'.$rowInfo['id']);
            $selectObj->setAttribute('rel'  , $rowInfo['id']);
            $selectObj->setAttribute('data-selected', $rowInfo[$fieldName]);

            $spanObj->addElement($selectObj);

            return $spanObj->getHtml();
            break;

         case 'event':
            $response = $fieldDesign['script']($rowInfo, $fieldDesign['parameters']=false);
            return $response;
            break;
      }
   }

   public function renderStatic($pagename) {
      $langInfo = translate_info();
      $pageView = str_replace('-','_', $pagename).'_'. $langInfo['curr'];

      if (!$this->_tplHandler->existsTemplate($pageView)) {
         ErrorHandler::getInstance()->showError('not-found');
         return;
      }

      $this->showMainMenu();

      $this->_tplHandler->useTemplate($pageView);
   }
}
